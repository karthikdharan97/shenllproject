package com.example.shenlltask;

import android.app.ActivityManager;
        import android.app.Notification;
        import android.app.NotificationChannel;
        import android.app.NotificationManager;
        import android.app.PendingIntent;
        import android.content.BroadcastReceiver;
        import android.content.Context;
        import android.content.Intent;
        import android.content.IntentFilter;
        import android.os.Build;
        import android.os.Bundle;
        import android.os.CountDownTimer;
        import android.util.Log;
        import android.widget.TextView;

        import java.sql.Time;
        import java.text.DecimalFormat;
        import java.text.NumberFormat;
        import java.util.Timer;

        import androidx.appcompat.app.AppCompatActivity;
        import androidx.core.app.NotificationCompat;


public class MainActivity extends AppCompatActivity {

    private NotificationManager mNotificationManager;
    private TextView mBuildVariantText;
    private String BuildVariant;
    private int mills = 300000;
    private CountDownTimer mTimer;
    Intent mServiceIntent;
    private BroadcastService mYourService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBuildVariantText = (TextView) findViewById(R.id.textview);
        BuildVariant = getPackageName();
        mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancelAll();


        if (BuildVariant.equals("com.example.shenlltask.PROD")) {
            mBuildVariantText.setText("BUILD VARIANTS " + "PROD");

        } else if (BuildVariant.equals("com.example.shenlltask.QA")) {
            mBuildVariantText.setText("BUILD VARIANTS " + "QA");
        } else if (BuildVariant.equals("com.example.shenlltask.DEV")) {
            mBuildVariantText.setText("BUILD VARIANTS " + "DEV");
        }
        Log.i("APPLICATION ENVIRONMENT", BuildVariant);
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("Service status", "Running");
                return true;
            }
        }
        Log.i("Service status", "Not running");
        return false;
    }


    @Override
    protected void onPause() {
        super.onPause();

        mYourService = new BroadcastService();
        mServiceIntent = new Intent(this, mYourService.getClass());
        if (!isMyServiceRunning(mYourService.getClass())) {
            startService(mServiceIntent);
        }



    }

    @Override
    protected void onDestroy() {

        stopService(mServiceIntent);
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("restartservice");
        broadcastIntent.setClass(this, Restarter.class);
        this.sendBroadcast(broadcastIntent);
        super.onDestroy();
    }
}




